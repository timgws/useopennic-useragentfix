<?php
$home = 'btn';
include('template.php');
echo $HEADER_TEMPLATE;
?> 
 <div class='hero ac'>
      <div class='container'>
        <h2>Democratic Censorship Free Browsing</h2>
        <div class='big-media'>
          <img alt='Fleet maintenance software, and so much more.' src='/img/heroimage.png' width='940' />
        </div>
      </div>
    </div>
    <div class='container content'>
      <div class='row'>
        <div class='span12'>
          <h1 class='lead'>
            Keep the internet you love, with a little extra content and a whole lot of extra freedom.
          </h1>
        </div>
        <div class='span4'>
          <p class='ac'>
            <a class='btn huge info' href='/setup.php' style='width:80%' title='Setup Your Computer For OpenNIC'>
              <strong>
                Get Started Now!
              </strong>
            </a>
          </p>
          <h3 class='ac' style='line-height: 1;'>
            No Software To Install
            <br />
            <small>
              Completely Free
            </small>
          </h3>
        </div>
      </div>
      <hr />
      <div class='row' id='why-opennic'>
        <div class='span8'>
          <div class='row'>
            <div class='span1'>
              <div class='benefit-icon' id='democracy-icon'></div>
            </div>
            <div class='span7'>
              <h3>Have A Say In Your DNS</h3>
              <p>
               The OpenNIC project is a democratic body which each user gets a vote when deciding new TLDs, policy changes, and the direction of the project.
              </p>
            </div>
          </div>
          <div class='row'>
            <div class='span1'>
              <div class='benefit-icon' id='nogovernment-icon'></div>
            </div>
            <div class='span7'>
              <h3>Freedom From Government Intervention</h3>
              <p>
               The Internet is not owned by any single government, corporation, or individual. OpenNIC takes DNS away from a centeral authority operated in cooperation with the US Federal Government, and moves it into the hands of the users of the Internet.
              </p>
            </div>
          </div>
          <div class='row'>
            <div class='span1'>
              <div class='benefit-icon' id='neutrality-icon'></div>
            </div>
            <div class='span7'>
              <h3>Total DNS Neutrality</h3>
              <p>
                No corporation can say what websites should or shouldn't load, and the speed in which they load. By using OpenNIC you no longer have to question your ISPs motives, and can rest assured that at least the websites you visit are not being throttled.
              </p>
            </div>
          </div>
          <div class='row'>
            <div class='span1'>
              <div class='benefit-icon' id='privacy-icon'></div>
            </div>
            <div class='span7'>
              <h3>Protect Your Privacy</h3>
              <p>
               Governments all over the world are looking for ways to capture your internet usage data. You choose the DNS server you connect to, what country it's located in, and how much logging is done.
              </p>
            </div>
          </div>
        </div>
        <div class='span8'>
          <div class='row'>
            <div class='span1'>
              <div class='benefit-icon' id='free-icon'></div>
            </div>
            <div class='span7'>
              <h3>No Cost (Gratis)</h3>
              <p>
                There are no plans to charge money for OpenNIC, it's free to use and the servers are run by volunteers (who go through a democratic approval process). No advertisements, no collection of data, and no monthly fees. 
              </p>
            </div>
          </div>
          <div class='row'>
            <div class='span1'>
              <div class='benefit-icon' id='tld-icon'></div>
            </div>
            <div class='span7'>
              <h3>New Top Level Domains</h3>
              <p>
                Unlock a whole new level of Internet! With OpenNIC you will have access to a constantly expanding range of new TLDs for you to visit and register.
              </p>
              <p>
                Think you have a Top Level Domain that you would like to add to OpenNIC? Then submit a request to the community, provide a server for the TLD to run on, and you are set!
              </p>
            </div>
          </div>
		  <div class='row'>
            <div class='span1'>
              <div class='benefit-icon' id='hijacking-icon'></div>
            </div>
            <div class='span7'>
              <h3>No More ISP DNS Hijacking</h3>
              <p>
                Have you ever typed a wrong URL into your browser and get redirected to a search page owned by your ISP? The domain you were typing in, the ads you click, and the search you input can all be collected by your ISP. This also is just annoying.
              </p>
			  <p>
			  With OpenNIC when you reach a URL that is invalid, then it lets your browser use it's default response to the error... instead of trying to decide what's best for you.
			  </p>
            </div>
          </div>
        </div>
      </div>
<?php
echo $FOOTER_TEMPLATE;
?>