<?php $setup = 'btn'; include('template.php'); echo $HEADER_TEMPLATE; 
error_reporting(0); if ($_GET['ipv4'] != "" || $_GET['ipv6'] != "") { if 
($_GET['country'] == 'bad' || $_GET['os'] == 'bad') { die("<div 
style='text-align:center; font-weight:bold; padding:30px;'>Please <a 
href='/setup.php'>Go Back</a> and fill out all fields</div>");
}
$os = $_GET['os'];
$country = $_GET['country'];
$linecount = 0;
if ($_GET['ipv6'] != NULL) {
$iptype = 'ipv6';
} else if ($_GET['ipv4'] != NULL) {
$iptype = 'ipv4';
}
echo "its working";
$file = fopen('http://sysadmin.geek.id.au/api.php', 'r');
while (($line = fgetcsv($file,100000,"|")) !== FALSE) {
  //$line is an array of the csv elements
  if ($line[8] == $country and $line[4] == $iptype and $line[2] == 'normal') {
	$availableip[$linecount]=$line;
	$linecount++;
	}

}
fclose($file);
//print_r($availableip);
$dnsservers = '';
$lines = 0;
while ($linecount > $lines && 4 > $lines) {
	$ipaddress = $availableip[$lines][3];
	$operator = $availableip[$lines][6];
	$region = $availableip[$lines][7];
	$country = $availableip[$lines][8];
	$logging = $availableip[$lines][11];
	$logginginfo = $availableip[$lines][12];
	$lines++;
	$dnsservers .= "<div class='alert-message info'><code>$ipaddress</code> - $region, $country Anonymous Logs: $logging (Info: $logginginfo )</div>";
}
if ($lines == 0) {
$dnsservers .= "There are currently no available DNS servers in this country, perhaps you can setup a server and help the cause? Go to <a href='http://opennicproject.org'>The OpenNIC Project Homepage</a>, to learn more. Or <a href='/setup.php'>Go Back</a> and select a new location.";
} else if ($lines == 1) {
$dnsservers .= "<blockquote>There is currently only one DNS server in this country, it is not required that you use multiple DNS servers but it helps incase one server goes down. Perhaps you can setup a server and help the cause? Go to <a href='http://opennicproject.org'>The OpenNIC Project Homepage</a>, to learn more. You can also <a href='/setup.php'>Go Back</a> and select a new location.</blockquote>";
}

if ($os == 'osx'){
$instructions = "
<h2>Step One</h2>
	<div class='well'>
		<p>Click on your <strong>'Apple'</strong> menu and choose <strong>'System Preference'</strong>:</p>
		<img src='/img/macosx1.jpg'>
	</div>
<h2>Step Two</h2>
	<div class='well'>
		<p>Click on the <strong>'Network'</strong> icon:</p>
		<img src='/img/macosx2.jpg'>
	</div>
<h2>Step Three</h2>
	<div class='well'>
		<p>Select the <strong>'DNS'</strong> tab at the top. This is where you can change your DNS settings, click the '+' arrow at the bottom and enter any two or three of the IPs into the <strong>'DNS Servers'</strong> box:</p>
$dnsservers
		<p>
Apply your settings by clicking the <strong>'Apply Now'</strong> or <strong>'OK'</strong> button and Done!:</p>
		<img src='/img/macosx4.jpg'>
	</div>

<h2>Step Four</h2>
	<div class='well'>
		<p>You are done! Let's test your connection.</p>
	</div>
	<a href='connection.php?completed' class='btn huge success' style='float:right;'>Check Your Connection</a>

";
} else if($os == 'win7') {
$instructions = "
<h2>Step One</h2>
	<div class='well'>
		<p>Click the Start menu, select Control Panel:</p>
		<img src='/img/windows71.jpg'>
	</div>
<h2>Step Two</h2>
	<div class='well'>
		<p>Find and Click on 'Network and Sharing Center':</p>
		<img src='/img/windows72.jpg'>
	</div>
<h2>Step Three</h2>
	<div class='well'>
		<p>On the right of the Window click on 'Change adapter settings':</p>
		<img src='/img/windows73.jpg'>
	</div>
<h2>Step Four</h2>
	<div class='well'>
		<p>Find your Network Adapter (Normally named 'Local Area Connection' and doesn\'t have TAP-Win32 or VirtualBox as the Device Name), Once Found Right-Clickon it and click 'Status':</p>
		<img src='/img/windows74.jpg'>
	</div>
<h2>Step Five</h2>
	<div class='well'>
		<p> On the Status Window click on the 'Details...' Button:</p>
		<img src='/img/windows75.jpg'>
	</div>
<h2>Step Six</h2>
	<div class='well'>
		<p>Now find 'IPv4 DNS Server', Next to it will be a IP Address note this down for later use, Then close the Details and Status Windows:</p>
		<img src='/img/windows76.jpg'>
	</div>
<h2>Step Seven</h2>
	<div class='well'>
		<p>Now you should be back at the Network Connections Window, Right-Click on your Network Adapter and click 'Properties' this time:</p>
		<img src='/img/windows77.jpg'>
	</div>
<h2>Step Eight</h2>
	<div class='well'>
		<p> Select 'Internet Protocol Version 4 (TCP/IPv4)' and Click 'Properties':</p>
		<img src='/img/windows78.jpg'>
	</div>
<h2>Step Nine</h2>
	<div class='well'>
		<p>Select 'Use the following DNS server addresses:':</p>
$dnsservers
		<img src='/img/windows79.jpg'>
	</div>
<h2>Step Ten</h2>
	<div class='well'>
		<p> Choose one of the following IP addresses and enter it into 'Preferred DNS server' and enter the Server from Step 6 as 'Alternate DNS server', Now click 'OK' to Save Changes:</p>
		
		<img src='/img/windows710.jpg'>
	</div>
<h2>Step Eleven</h2>
	<div class='well'>
				<p>You are done! Let\'s test your connection.</p>
	</div>
	<a href='connection.php?completed' class='btn huge success' style='float:right;'>Check Your Connection</a>
";
	} else if($os == 'winxp') {
$instructions = "
<p>Sorry this tutorial has limited images for now</p>
<h1>High Speed</h1>
<h2>Step One</h2>
	<div class='well'>
		<p>Go to Control Panel>Network Connections and select your local network.</p>
	</div>
<h2>Step Two</h2>
	<div class='well'>
		<p>Click Properties, then select Internet Protocol (TCP/IP).</p>
	</div>
<h2>Step Three</h2>
	<div class='well'>

		<p>Click Properties. You will see a window like the one below - this is the Internet Protocol window. Select 'Use the following DNS server addresses' and enter the desired DNS server(s) in the space(s) provided.</p>
		$dnsservers
	</div>

<h2>Step Four</h2>
	<div class='well'>
				<p>You are done! Let\'s test your connection.</p>
	</div>
	<a href='connection.php?completed' class='btn huge success' style='float:right;'>Check Your Connection</a>

<h1>Dial Up</h1>
<h2>Step One</h2>
	<div class='well'>
		<p>Go to My Computer>Dialup Networking.</p>
	</div>
<h2>Step Two</h2>
	<div class='well'>
		<p>Right-click your internet connection and select Properties.</p>
	</div>
<h2>Step Three</h2>
	<div class='well'>
		<p>A window will open - click the Server Types tab. Click TCP/IP Settings. You will see a window like the one below - this is the Internet Protocol window. Select 'Use the following DNS server addresses' and enter the desired DNS server(s) in the space(s) provided.</p>
		$dnsservers
	</div>
	
<h2>Step Four</h2>
	<div class='well'>

				<p>You are done! Let\'s test your connection.</p>
	</div>
	<a href='connection.php?completed' class='btn huge success' style='float:right;'>Check Your Connection</a>
";
	} else if($os == 'linux') {
$instructions = "
<h2>Step One</h2>
	<div class='well'>
		<p>Updating your linux machine to use OpenNIC is very easy: Start by opening up a terminal (or by dropping to shell) and type 'su' or if using a debian based system 'sudo -i' and put in your password. After you are root, copy the following into terminal.</p>
 
<pre>cp /etc/resolv.conf /etc/resolv.conf.orig</pre>
 
<p>then</p>
 
<pre>nano /etc/resolv.conf</pre>
<p>(you can use \'gedit\', \'vim\', or any other standard text editor as well):</p>
		<img src='/img/linux1.jpg'>
	</div>
	
<h2>Step Two</h2>
	<div class='well'>
		<ul>
			<li>Leave the lines starting with domain and search</li>
			<li>Modify the nameserver to one of the IP addresses below</li>
			<li>Save the file and exit (ctrl+o -> enter -> ctrl+x in nano)</li>
		</ul>
		$dnsservers
		<img src='/img/linux2.jpg'>
	</div>
<h2>Step Three</h2>
	<div class='well'>
	<p>Type in the following into your terminal</p>
	<pre>nano /etc/dhcp3/dhclient.conf</pre>
	<p>Scroll to the bottom of the file and add the following line:</p>
	<pre>prepend domain-name-servers DNS1, DNS2, DNS3;</pre>
<p>Replace DNS1, DNS2, and DNS3 with the following IP addresses:</p>
$dnsservers
		<p>Scroll all the way to the bottom, and add the line there.</p>
		<img src='/img/linux3.jpg'>
	</div>
<h2>Step Four</h2>
		<div class='well'>
				<p>You are done! Let\'s test your connection.</p>
	</div>
	<a href='connection.php?completed' class='btn huge success' style='float:right;'>Check Your Connection</a>";
	}
?>
<div class="hero">
      <div class="container">
        <h1 style='line-height:65px;'>Step By Step Instructions</h1>
      </div>
    </div>
<div class="container content" style="margin-bottom:50px;">
<?php echo $instructions; ?>
</div>
<?php
} else {
function getOS($userAgent) {
  // Create list of operating systems with operating system name as array key 
	$oses = array (
		'iPhone' => '(iPhone)',
		'Windows 3.11' => 'Win16',
		'Windows 95' => '(Windows 95|Win95|Windows_95)', // Use regular expressions as value to identify operating system
		'Windows 98' => '(Windows 98|Win98)',
		'Windows 2000' => '(Windows NT 5.0|Windows 2000)',
		'Windows XP' => '(Windows NT 5.1|Windows XP)',
		'Windows 2003' => '(Windows NT 5.2)',
		'Windows Vista' => '(Windows NT 6.0|Windows Vista)',
		'Windows 7' => '(Windows NT 6.1|Windows 7)',
		'Windows NT 4.0' => '(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)',
		'Windows ME' => 'Windows ME',
		'Open BSD'=>'OpenBSD',
		'Sun OS'=>'SunOS',
		'Linux'=>'(Linux|X11)',
		'Mac OS X'=>'(Mac_PowerPC|Macintosh)',
		'Safari' => '(Safari)',
		'QNX'=>'QNX',
		'BeOS'=>'BeOS',
		'OS/2'=>'OS/2',
		'Search Bot'=>'(nuhk|Googlebot|Yammybot|Openbot|Slurp/cat|msnbot|ia_archiver)'
	);

	foreach($oses as $os=>$pattern){ // Loop through $oses array
    // Use regular expressions to check operating system type
		if(preg_match('%' . str_replace('.', '\.', $pattern) . '%i', $userAgent)) { // Check if a value in $oses array matches current user agent.
			return $os; // Operating system was matched so return $oses key
		}
	}
	return 'Unknown'; // Cannot find operating system so return Unknown
}




?> 
<form action='setup.php'>
<div class="hero">
      <div class="container">
        <h1>Setup Your Computer</h1>
      </div>
    </div>
<div class="container content" style="margin-bottom:50px;">
	<div class="well">
		<h2>What Country To Host Your DNS?<small>Choose the closest country to you (usually)</small></h2>
		<p>Your country was matched as: <strong>
			<?php
			$IP = $_SERVER['REMOTE_ADDR'];
			$SSID = htmlentities(SID);
			if (!empty($IP)) {
			  $country=file_get_contents('http://api.hostip.info/get_html.php?ip='.$IP);
			list ($_country) = explode ("\n", $country);
            $_country = str_replace("Country: ", "", $country);
            echo $_country;
			}
			?>
		</strong></p>
		<div class="control-group">
            <label class="control-label" for="country">Country </label>
            <div class="controls">
              <select id="country" name="country">
                <option value="bad">Please Select</option>
                <option value="US">United States</option>
                <option value="CA">Canada</option>
                <option value="DE">Germany</option>
                <option value="UK">United Kingdom</option>
		<option value="SE">Sweden</option>
		<option value="FR">France</option>
		<option value="AU">Australia</option>
		<option value="NL">Netherlands</option>
		<option value="LU">Luxembourg</option>
		<option value="NZ">New Zealand</option>
              </select>
            </div>
          </div>
	</div>
	<hr/>
	<div class="well">
		<h2>What Operating System Do You Run?</h2>
		<p>Your Operating System was matched as: <strong><?php echo getOS($_SERVER['HTTP_USER_AGENT']); ?></strong></p>
		<div class="control-group">
            <label class="control-label" for="os">Operating System </label>
            <div class="controls">
              <select id="os" name="os">
                <option value='bad'>Please Select</option>
                <option value='winxp'>Windows XP</option>
                <option value='win7'>Windows 7 / Vista</option>
				<option value='osx'>Mac OS X</option>
				<option value='linux'>Linux</option>
              </select>
            </div>
          </div>
	</div>
	<hr/>
	<button class='btn huge success' style="float:right" type="submit" name="ipv4" value="true">Lets Do It!</button>
	 <button class='btn huge info' style="float:right" type="submit" name="ipv6" value="true">IPv6</button>
</form>
</div>
<?php
}
?>
