<?php
$HEADER_TEMPLATE = "<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta content='text/html; charset=utf-8' http-equiv='Content-Type' />
    <meta content='OpenNIC is a free democratic, censorship free, and community run DNS solution! Easy to setup!' />
    <title>Use OpenNIC - The Democratic, Censorship Free, DNS Solution.</title>
    <!--[if lt IE 9]>
      <script src='//html5shim.googlecode.com/svn/trunk/html5.js'></script>
    <![endif]-->
    <link href='//fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css' />
    <link href='css/style-default.css' media='all' rel='stylesheet' type='text/css' />
    <link href='css/style-print.css' media='print' rel='stylesheet' type='text/css' />
	  <script src='//ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js'></script>
    <script src='/js/bootstrap.min.js'></script>
	<script src='/js/bootstrap-tooltip.js'></script>
<script type='text/javascript'>

function revisar(){

img_error = 'http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_invalido.png';
img_cargando = 'http://maximi89.linuxerz.org/wp-content/uploads/2011/07/opennic_flor.gif';

reload = new Date();
reload = reload.getTime();
//?nocache='+reload //se pone al final de las imágenes
//?state=numero


///////IMAGENES VALIDOS///////////
img_bbs		= 'http://www.opennictest.bbs/opennic_valido.png?'+ new Date().getTime();
//img_gopher	= 'http://www.opennictest.gopher/opennic_valido.png?'+ new Date().getTime();
img_gopher	= 'http://pongonova.gopher/gopherwiki/images/opennic_valido.png?'+ new Date().getTime();
img_fur		= 'http://www.opennictest.fur/opennic_valido.png?'+ new Date().getTime();
img_free	= 'http://www.opennictest.free/opennic_valido.png?'+ new Date().getTime();
img_geek	= 'http://www.opennictest.geek/opennic_valido.png?'+ new Date().getTime();
img_indy	= 'http://www.opennictest.indy/opennic_valido.png?'+ new Date().getTime();
img_null	= 'http://www.opennictest.null/opennic_valido.png?'+ new Date().getTime();
//img_oss 	= 'http://www.opennictest.oss/opennic_valido.png?'+ new Date().getTime();
img_oss		= 'http://opennic.oss/files/opennic_valido.png?'+ new Date().getTime();
//img_parody 	= 'http://www.opennictest.parody/opennic_valido.png?'+ new Date().getTime();
img_parody	= 'http://opennic.parody/files/opennic_valido.png?'+ new Date().getTime();
img_micro 	= 'http://www.opennictest.micro/opennic_valido.png?'+ new Date().getTime();
img_ing		= 'http://www.opennictest.ing/opennic_valido.png?'+ new Date().getTime();
img_dyn 	= 'http://www.opennictest.dyn/opennic_valido.png?'+ new Date().getTime();
img_p2p 	= 'http://www.opennictest.p2p/opennic_valido.png?'+ new Date().getTime();
img_bit 	= 'http://www.opennictest.bit/opennic_valido.png?'+ new Date().getTime();
img_bzh 	= 'http://www.opennictest.bzh/opennic_valido.png?'+ new Date().getTime();
///////IMAGENES VALIDOS IPV6/////////
img_bbs_ipv	= 'http://ipv6.opennictest.bbs/opennic_valido.png';
img_gopher_ipv	= 'http://ipv6.opennictest.gopher/opennic_valido.png';
img_fur_ipv 	= 'http://ipv6.opennictest.fur/opennic_valido.png';
img_free_ipv	= 'http://ipv6.opennictest.free/opennic_valido.png';
img_geek_ipv	= 'http://ipv6.opennictest.geek/opennic_valido.png';
img_indy_ipv	= 'http://ipv6.opennictest.indy/opennic_valido.png';
img_null_ipv	= 'http://ipv6.opennictest.null/opennic_valido.png';
img_oss_ipv 	= 'http://ipv6.opennictest.oss/opennic_valido.png';
img_parody_ipv	= 'http://ipv6.opennictest.parody/opennic_valido.png';
img_micro_ipv	= 'http://ipv6.opennictest.micro/opennic_valido.png';
img_ing_ipv	= 'http://ipv6.opennictest.ing/opennic_valido.png';
img_dyn_ipv 	= 'http://ipv6.opennictest.dyn/opennic_valido.png';
img_p2p_ipv	= 'http://ipv6.opennictest.p2p/opennic_valido.png';
img_bit_ipv 	= 'http://ipv6.opennictest.bit/opennic_valido.png';
img_bzh_ipv 	= 'http://ipv6.opennictest.bzh/opennic_valido.png';



///////CAMBIA COLOR A NEGRO DEL TITULO OPENNIC///////////////
///////document.getElementById('titulo').style.color='grey';

///////IMAGENES CARGANDO DURANTE LA REVISION////////////////
document.getElementById('imagen_bbs').src = img_cargando;
document.getElementById('imagen_gopher').src = img_cargando;
document.getElementById('imagen_fur').src = img_cargando;
document.getElementById('imagen_free').src = img_cargando;
document.getElementById('imagen_geek').src = img_cargando;
document.getElementById('imagen_indy').src = img_cargando;
document.getElementById('imagen_null').src = img_cargando;
document.getElementById('imagen_oss').src = img_cargando;
document.getElementById('imagen_parody').src = img_cargando;
document.getElementById('imagen_micro').src = img_cargando;
document.getElementById('imagen_ing').src = img_cargando;


var i = new Image();
i.onload = function() { document.getElementById('imagen_bbs').src=img_bbs; document.getElementById('titulo').style.color='#33FF00';}
i.onerror = function() { document.getElementById('imagen_bbs').src = img_error; }
i.src = img_bbs;
var i = new Image();
i.onload = function() { document.getElementById('imagen_bbs_ipv').src = img_bbs_ipv;}
i.onerror = function() { document.getElementById('imagen_bbs_ipv').src = img_error; }
i.src = img_bbs_ipv;


var i = new Image();
i.onload = function() { document.getElementById('imagen_free').src = img_free; document.getElementById('titulo').style.color='#33FF00';}
i.onerror = function() { document.getElementById('imagen_free').src = img_error; }
i.src = img_free;
var i = new Image();
i.onload = function() { document.getElementById('imagen_free_ipv').src = img_free_ipv;}
i.onerror = function() { document.getElementById('imagen_free_ipv').src = img_error; }
i.src = img_free_ipv;


var i = new Image();
i.onload = function() { document.getElementById('imagen_fur').src = img_fur; document.getElementById('titulo').style.color='#33FF00';}
i.onerror = function() { document.getElementById('imagen_fur').src = img_error; }
i.src = img_fur;
var i = new Image();
i.onload = function() { document.getElementById('imagen_fur_ipv').src = img_fur_ipv;}
i.onerror = function() { document.getElementById('imagen_fur_ipv').src = img_error; }
i.src = img_fur_ipv;


var i = new Image();
i.onload = function() { document.getElementById('imagen_geek').src = img_geek; document.getElementById('titulo').style.color='#33FF00';}
i.onerror = function() { document.getElementById('imagen_geek').src = img_error; }
i.src = img_geek;
var i = new Image();
i.onload = function() { document.getElementById('imagen_geek_ipv').src = img_geek_ipv;}
i.onerror = function() { document.getElementById('imagen_geek_ipv').src = img_error; }
i.src = img_geek_ipv;


var i = new Image();
i.onload = function() { document.getElementById('imagen_gopher').src = img_gopher;document.getElementById('titulo').style.color='#33FF00';}
i.onerror = function() { document.getElementById('imagen_gopher').src = img_error; }
i.src = img_gopher;
var i = new Image();
i.onload = function() { document.getElementById('imagen_gopher_ipv').src = img_gopher_ipv;}
i.onerror = function() { document.getElementById('imagen_gopher_ipv').src = img_error; }
i.src = img_gopher_ipv;


var i = new Image();
i.onload = function() { document.getElementById('imagen_indy').src = img_indy; document.getElementById('titulo').style.color='#33FF00';}
i.onerror = function() { document.getElementById('imagen_indy').src = img_error; }
i.src = img_indy;
var i = new Image();
i.onload = function() { document.getElementById('imagen_indy_ipv').src = img_indy_ipv;}
i.onerror = function() { document.getElementById('imagen_indy_ipv').src = img_error; }
i.src = img_indy_ipv;


var i = new Image();
i.onload = function() { document.getElementById('imagen_ing').src = img_ing; document.getElementById('titulo').style.color='#33FF00';}
i.onerror = function() { document.getElementById('imagen_ing').src = img_error; }
i.src = img_ing;
var i = new Image();
i.onload = function() { document.getElementById('imagen_ing_ipv').src = img_ing_ipv;}
i.onerror = function() { document.getElementById('imagen_ing_ipv').src = img_error; }
i.src = img_ing_ipv;


var i = new Image();
i.onload = function() { document.getElementById('imagen_null').src = img_null; document.getElementById('titulo').style.color='#33FF00';}
i.onerror = function() { document.getElementById('imagen_null').src = img_error; }
i.src = img_null;
var i = new Image();
i.onload = function() { document.getElementById('imagen_null_ipv').src = img_null_ipv;}
i.onerror = function() { document.getElementById('imagen_null_ipv').src = img_error; }
i.src = img_null_ipv;


var i = new Image();
i.onload = function() { document.getElementById('imagen_oss').src = img_oss; document.getElementById('titulo').style.color='#33FF00';}
i.onerror = function() { document.getElementById('imagen_oss').src = img_error; }
i.src = img_oss;
var i = new Image();
i.onload = function() { document.getElementById('imagen_oss_ipv').src = img_oss_ipv;}
i.onerror = function() { document.getElementById('imagen_oss_ipv').src = img_error; }
i.src = img_oss_ipv;


var i = new Image();
i.onload = function() { document.getElementById('imagen_micro').src = img_micro; document.getElementById('titulo').style.color='#33FF00';}
i.onerror = function() { document.getElementById('imagen_micro').src = img_error; }
i.src = img_micro;
var i = new Image();
i.onload = function() { document.getElementById('imagen_micro_ipv').src = img_micro_ipv;}
i.onerror = function() { document.getElementById('imagen_micro_ipv').src = img_error; }
i.src = img_micro_ipv;


var i = new Image();
i.onload = function() { document.getElementById('imagen_parody').src = img_parody;document.getElementById('titulo').style.color='#33FF00';}
i.onerror = function() { document.getElementById('imagen_parody').src = img_error; }
i.src = img_parody;
var i = new Image();
i.onload = function() { document.getElementById('imagen_parody_ipv').src = img_parody_ipv;}
i.onerror = function() { document.getElementById('imagen_parody_ipv').src = img_error; }
i.src = img_parody_ipv;

/*var i = new Image();
i.onload = function() { document.getElementById('imagen_dyn').src = img_dyn; document.getElementById('titulo').style.color='#33FF00';}
i.onerror = function() { document.getElementById('imagen_dyn').src = img_error; }
i.src = img_dyn;
var i = new Image();
i.onload = function() { document.getElementById('imagen_dyn_ipv').src = img_dyn_ipv;}
i.onerror = function() { document.getElementById('imagen_dyn_ipv').src = img_error; }
i.src = img_dyn_ipv;

var i = new Image();
i.onload = function() { document.getElementById('imagen_p2p').src = img_p2p; document.getElementById('titulo').style.color='#33FF00';}
i.onerror = function() { document.getElementById('imagen_p2p').src = img_error; }
i.src = img_p2p;
var i = new Image();
i.onload = function() { document.getElementById('imagen_p2p_ipv').src = img_p2p_ipv;}
i.onerror = function() { document.getElementById('imagen_p2p_ipv').src = img_error; }
i.src = img_p2p_ipv;

var i = new Image();
i.onload = function() { document.getElementById('imagen_bit').src = img_bit; document.getElementById('titulo').style.color='#33FF00';}
i.onerror = function() { document.getElementById('imagen_bit').src = img_error; }
i.src = img_bit;
var i = new Image();
i.onload = function() { document.getElementById('imagen_bit_ipv').src = img_bit_ipv;}
i.onerror = function() { document.getElementById('imagen_bit_ipv').src = img_error; }
i.src = img_bit_ipv;

var i = new Image();
i.onload = function() { document.getElementById('imagen_bzh').src = img_bzh; document.getElementById('titulo').style.color='#33FF00';}
i.onerror = function() { document.getElementById('imagen_bzh').src = img_error; }
i.src = img_bzh;
var i = new Image();
i.onload = function() { document.getElementById('imagen_bzh_ipv').src = img_bzh_ipv;}
i.onerror = function() { document.getElementById('imagen_bzh_ipv').src = img_error; }
i.src = img_bzh_ipv;
*/

}
</script>
  </head>
  <body id='home'>
<script> setTimeout('revisar()', 2000); </script>
    <div class='topbar'>
      <div class='header'>
        <div class='container'>
          <a class='brand' href='/' title='Converting People To OpenNIC Since 2012'>
            <img alt='OpenNIC' height='28' src='/img/logo.png'/>
          </a>
          <h1 class='site-title'>
            <a href='/' title='The OpenNIC Awareness Project'>
              Use OpenNIC
            </a>
          </h1>
          <ul class='nav'>
            <li>
              <a class='$home' href='/'>
                Home
              </a>
            </li>
            <li>
              <a class='$setup' href='/setup.php'>
                Get Started
              </a>
            </li>
            <li>
              <a class='$connection' href='/connection.php'>
                Test Connection 
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>";
	
$FOOTER_TEMPLATE = "      <hr />
      <blockquote class='big-quote'>
        <h2>
          Universal access to human knowledge is in our grasp, for the first time in the history of the world. This is not a bad thing.
        </h2>
        <small>
          <a href='http://www.craphound.com/' target='_blank' title='Cory Doctorow'>
            Cory Doctorow
          </a>
        </small>
      </blockquote>
      <div class='well ac'>
        <h3>
		<a href='/setup.php' title='Setup Your Computer For OpenNIC'>
            Get Started
          </a>
          or
          <a href='/connection.php' title='Test Your Connection To OpenNIC Root'>
            Test Your Connection
          </a>
        </h3>
      </div>
    </div>
  </body>
</html>";
?>
