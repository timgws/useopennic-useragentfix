<?php
$connection = 'btn';
include('template.php');
?> 

<div class="hero">
      <div class="container">
        <h1 style='text-align:center;line-height:45px;'>Test Your Connection</h1>
      </div>
    </div>
<div class="container content">
      <div class="row">
        <div class="span9">
          <h2>Let's See If You Are Connected!</h2>
          <p>
            On the right of the page, you should see an image saying that you are successfully connected to the OpenNIC root. If you see a broken image you should attempt to follow these trouble shooting steps.
          </p>
          <h4>Restart Your Computer</h4>
		  <p>
            A common problem is that your DNS is cached on your computer or that your settings haven't been changed throughout all your programs. So if you restart your computer it may fix the problem.
          </p>
		  <h4>Check Your Settings</h4>
		  <p>
			Try going through our setup tool again and making sure that all your settings are proper to your computer.
		  </p>
		  <p>
            <a class="btn" href="setup.php">
              Setup Your Computer!
            </a>
          </p>
		  <h4>Ensure Your ISP Does Not Intercept DNS</h4>
          <p>
            Some ISPs actually capture DNS on their end, and then run your queries through their own DNS system. If this is the case you should contact them directly and ask why they are forcing their DNS choice upon you.</p>
		<p>Our System Says Your ISP Is:<?php
		 $ip = $_SERVER['REMOTE_ADDR'];
		$fullhost = gethostbyaddr($ip);
		$host = preg_replace("/^[^.]+./", "", $fullhost);
echo $host;
?>
          </p>
        </div>
        <div class="span7">
          <hr>
           <div class="row">
            <div class="span2">
              <p>
                <img alt="Connection Test" height="114" src="http://useopennic.free/img/connection.jpg" width="100">
              </p>
            </div>
            <div class="span5">
              <p>
                You should see an image saying "You Are Connected".
              </p>
            </div>
          
  
          </div>
		  <hr>
          &nbsp;
        </div>
      </div>
    </div>
	<?php
echo $FOOTER_TEMPLATE;
?>